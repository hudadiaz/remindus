class CreateIgnoreReminders < ActiveRecord::Migration
  def change
    create_table :ignore_reminders do |t|
      t.references :user, index: true
      t.references :reminder, index: true

      t.timestamps
    end
  end
end
