class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.references :user, index: true
      t.string :name
      t.string :description
      t.string :password
      t.boolean :is_public

      t.timestamps
    end
  end
end
