class CreateReminders < ActiveRecord::Migration
  def change
    create_table :reminders do |t|
      t.references :user, index: true
      t.references :channel, index: true
      t.string :title
      t.string :note
      t.datetime :start
      t.datetime :end
      t.boolean :is_public
      t.boolean :repeated

      t.timestamps
    end
  end
end
