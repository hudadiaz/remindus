class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :channels, :dependent => :destroy
  has_many :subscriptions, :dependent => :destroy
  has_many :reminders, :dependent => :destroy
  has_many :ignore_reminders, :dependent => :destroy
  has_many :subscribed_channels, through: :subscriptions, source: :channel
  has_many :subscription_reminders, through: :subscriptions, source: :reminders
  has_many :channel_reminders, through: :channels, source: :reminders
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def all_channels
    (channels + subscribed_channels).uniq
  end

  def all_reminders
    (subscription_reminders + channel_reminders).uniq
  end
end
