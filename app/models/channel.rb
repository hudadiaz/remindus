class Channel < ActiveRecord::Base
  belongs_to :user
  has_many :subscription, :dependent => :destroy
  has_many :reminders, :dependent => :destroy
  validates_presence_of :name, :user_id

  def self.search(search, user)
    if search
      where("user_id != ? AND is_public = true AND name LIKE ?", user.id, "%#{search}%")
    else
      where("user_id != ? AND is_public = true", user.id)
    end
  end
end
