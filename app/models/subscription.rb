class Subscription < ActiveRecord::Base
  belongs_to :user
  belongs_to :channel
  has_many :reminders, through: :channel
end
