class Reminder < ActiveRecord::Base
  belongs_to :user
  belongs_to :channel
  validates_presence_of :title, :user_id, :channel_id, :start, :end

  before_save do
    self.start = self.start.localtime
    self.end = self.end.localtime
  end

  validate :end_date_cannot_be_in_the_past

  def end_date_cannot_be_in_the_past
    errors.add(:end, "cannot be in the past") if self.end < Date.today
  end

  validate :end_date_cannot_be_before_start

  def end_date_cannot_be_before_start
    errors.add(:end, "must end after start") if self.end <= self.start
  end
end
