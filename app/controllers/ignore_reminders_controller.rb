class IgnoreRemindersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_ignore_reminder, only: [:show, :edit, :update, :destroy]

  # GET /ignore_reminders
  # GET /ignore_reminders.json
  def index
    @ignore_reminders = IgnoreReminder.all
  end

  # GET /ignore_reminders/1
  # GET /ignore_reminders/1.json
  def show
  end

  # GET /ignore_reminders/new
  def new
    @ignore_reminder = IgnoreReminder.new
  end

  # GET /ignore_reminders/1/edit
  def edit
  end

  # POST /ignore_reminders
  # POST /ignore_reminders.json
  def create
    @ignore_reminder = IgnoreReminder.new(ignore_reminder_params)

    respond_to do |format|
      if @ignore_reminder.save
        format.html { redirect_to @ignore_reminder, notice: 'Ignore reminder was successfully created.' }
        format.json { render :show, status: :created, location: @ignore_reminder }
      else
        format.html { render :new }
        format.json { render json: @ignore_reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ignore_reminders/1
  # PATCH/PUT /ignore_reminders/1.json
  def update
    respond_to do |format|
      if @ignore_reminder.update(ignore_reminder_params)
        format.html { redirect_to @ignore_reminder, notice: 'Ignore reminder was successfully updated.' }
        format.json { render :show, status: :ok, location: @ignore_reminder }
      else
        format.html { render :edit }
        format.json { render json: @ignore_reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ignore_reminders/1
  # DELETE /ignore_reminders/1.json
  def destroy
    @ignore_reminder.destroy
    respond_to do |format|
      format.html { redirect_to ignore_reminders_url, notice: 'Ignore reminder was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ignore_reminder
      @ignore_reminder = IgnoreReminder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ignore_reminder_params
      params.require(:ignore_reminder).permit(:user_id, :reminder_id)
    end
end
