class RemindersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_reminder, only: [:show, :edit, :update, :destroy]
  before_action :set_channel, only: [:new, :show, :edit, :update, :destroy]
  before_filter :validate_owner, only: [:edit, :update, :destroy]

  # GET /reminders
  # GET /reminders.json
  def index
    @reminders = current_user.all_reminders.uniq
  end

  # GET /reminders/1
  # GET /reminders/1.json
  def show
  end

  # GET /reminders/new
  def new
    @reminder = @channel.reminders.new
    @reminder.is_public = @channel.is_public
    @reminder.start = Time.now
    @reminder.end = Time.now + 3600
  end

  # GET /reminders/1/edit
  def edit
  end

  # POST /reminders
  # POST /reminders.json
  def create
    @channel = Channel.find params[:channel_id]
    @reminder = @channel.reminders.create(reminder_params)
    @reminder.user_id = current_user.id

    respond_to do |format|
      if @reminder.save
        format.html { redirect_to [@channel, @reminder], notice: 'Reminder was successfully created.' }
        format.json { render :show, status: :created, location: @reminder }
      else
        format.html { render :new }
        format.json { render json: @reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reminders/1
  # PATCH/PUT /reminders/1.json
  def update
    respond_to do |format|
      if @reminder.update(reminder_params)
        format.html { redirect_to [@reminder.channel, @reminder], notice: 'Reminder was successfully updated.' }
        format.json { render :show, status: :ok, location: @reminder }
      else
        format.html { render :edit }
        format.json { render json: @reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reminders/1
  # DELETE /reminders/1.json
  def destroy
    channel = @reminder.channel
    @reminder.destroy
    respond_to do |format|
      format.html { redirect_to channel_path(channel), notice: 'Reminder was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reminder
      @reminder = Reminder.find(params[:id])
    end

    def set_channel
      @channel = Channel.find params[:channel_id]
      if @channel == nil        
        @channel = @reminder.channel
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reminder_params
      params.require(:reminder).permit(:channel_id, :title, :note, :start, :end, :is_public, :repeated)
    end

    def validate_owner
      unless @reminder.user == current_user || @channel.user == current_user
        redirect_to @channel
      end
    end
end
