class SubscriptionsController < ApplicationController
  before_action :authenticate_user!

  # GET /subscriptions
  # GET /subscriptions.json
  def index
    @subscriptions = Subscription.all
  end

  # GET /subscriptions/new
  def new
    @channels = Channel.search(params[:search], current_user)
    @subscription = current_user.subscriptions.new
  end

  # POST /subscriptions
  # POST /subscriptions.json
  def create
    @subscription = current_user.subscriptions.create(subscription_params)

    respond_to do |format|
      if @subscription.save
        format.html { redirect_to @subscription.channel, notice: 'Subscription was successfully created.' }
        format.json { render :show, status: :created, location: @subscription }
      else
        format.html { render :new }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subscriptions/1
  # DELETE /subscriptions/1.json
  def destroy
    @subscription = Subscription.find(params[:id])
    if @subscription.user != current_user
      redirect_to subscriptions
    end
    @subscription.destroy
    respond_to do |format|
      format.html { redirect_to subscriptions_url, notice: 'Subscription was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def subscription_params
      params.require(:subscription).permit(:channel_id)
    end
end
