json.array!(@channels) do |channel|
  json.extract! channel, :id, :user_id, :name, :description, :password, :is_public
  json.url channel_url(channel, format: :json)
end
