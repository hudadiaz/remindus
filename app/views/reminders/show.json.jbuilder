json.extract! @reminder, :id, :user_id, :channel_id, :title, :note, :start, :end, :is_public, :repeated, :created_at, :updated_at
