json.array!(@reminders) do |reminder|
  json.extract! reminder, :id, :user_id, :channel_id, :title, :note, :start, :end, :is_public, :repeated
  json.url reminder_url(reminder, format: :json)
end
