json.array!(@ignore_reminders) do |ignore_reminder|
  json.extract! ignore_reminder, :id, :user_id, :reminder_id
  json.url ignore_reminder_url(ignore_reminder, format: :json)
end
