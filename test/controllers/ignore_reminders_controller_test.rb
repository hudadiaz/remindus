require 'test_helper'

class IgnoreRemindersControllerTest < ActionController::TestCase
  setup do
    @ignore_reminder = ignore_reminders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ignore_reminders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ignore_reminder" do
    assert_difference('IgnoreReminder.count') do
      post :create, ignore_reminder: { reminder_id: @ignore_reminder.reminder_id, user_id: @ignore_reminder.user_id }
    end

    assert_redirected_to ignore_reminder_path(assigns(:ignore_reminder))
  end

  test "should show ignore_reminder" do
    get :show, id: @ignore_reminder
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ignore_reminder
    assert_response :success
  end

  test "should update ignore_reminder" do
    patch :update, id: @ignore_reminder, ignore_reminder: { reminder_id: @ignore_reminder.reminder_id, user_id: @ignore_reminder.user_id }
    assert_redirected_to ignore_reminder_path(assigns(:ignore_reminder))
  end

  test "should destroy ignore_reminder" do
    assert_difference('IgnoreReminder.count', -1) do
      delete :destroy, id: @ignore_reminder
    end

    assert_redirected_to ignore_reminders_path
  end
end
